FROM openjdk:8-jre-alpine
COPY build/libs/*.jar /opt/routine/lib/app.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/routine/lib/app.jar"]
