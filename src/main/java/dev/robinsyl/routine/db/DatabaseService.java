package dev.robinsyl.routine.db;

import dev.robinsyl.routine.types.*;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class DatabaseService {

    private Connection con;

    public DatabaseService() {
        final String url = "jdbc:postgresql://postgres-routine:5432/routine";
        final String user = "routine";
        final String password = "";

        int tries = 1;
        int timeout = 500;
        while (true) {
            try {
                con = DriverManager.getConnection(url, user, password);
                break;
            } catch (SQLException e) {
                System.err.println("Could not get connection, try " + tries);
            }
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            }
            timeout *= 2;
            if (tries++ > 10) {
                System.err.println("Could not get connection, giving up.");
                return;
            }
        }
        System.out.println("Connected to database");
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate("create table if not exists users (id serial constraint users_pk primary key, name varchar(32) not null);");
            stmt.executeUpdate("create table if not exists lists (id serial constraint lists_pk primary key constraint lists_users_id_fk references users on update cascade on delete cascade, name varchar(32) not null, owner int not null);");
            stmt.executeUpdate("create table if not exists items (id serial constraint items_pk primary key constraint items_lists_id_fk references lists on update cascade on delete cascade, name varchar(32) not null, date date default CURRENT_DATE not null, completed boolean default false not null, frequency smallint default 1 not null, parent int);");
            stmt.executeUpdate("create table if not exists history (item_id int constraint history_items_id_fk references items on update cascade on delete cascade, date date, completed boolean not null, constraint history_pk primary key (item_id, date));");
            System.out.println("Database has been initialised.");
        } catch (SQLException e) {
            System.out.println("Could not initialise database");
        }
    }

    public Optional<User> newUser(String name) {
        int id = 0;
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO users(name) VALUES(?);")) {
            stmt.setString(1, name);
            int rows = stmt.executeUpdate();
            if (rows == 1) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    } else {
                        return Optional.empty();
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in newUser!");
        }
        return getUser(id);
    }

    public Optional<User> updateUser(int userId, String name) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE users SET name=? WHERE id=?;")) {
            stmt.setString(1, name);
            stmt.setInt(2, userId);
            int rows = stmt.executeUpdate();
            if (rows != 1) {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.err.println("Database error in updateUser!");
        }
        return getUser(userId);
    }

    public Set<User> getUsers() {
        Set<User> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name FROM users;")) {
            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String name = rs.getString("name");
                        results.add(new User(id, name));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in getUsers!");
            return Collections.emptySet();
        }
        return results;
    }

    public Optional<User> getUser(int userId) {
        User user;
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name FROM users WHERE id=?;")) {
            stmt.setInt(1, userId);
            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }

            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }

                int id = rs.getInt("id");
                String name = rs.getString("name");
                user = new User(id, name);
            }
        } catch (SQLException e) {
            System.err.println("Database error in getUser!");
            return Optional.empty();
        }
        return Optional.of(user);
    }

    public Set<ItemList> getLists(int userId) {
        Set<ItemList> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name FROM lists WHERE owner=?;")) {
            stmt.setInt(1, userId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String name = rs.getString("name");
                        results.add(new ItemList(id, name));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in getLists!");
            return Collections.emptySet();
        }
        return results;
    }

    public Optional<ItemList> newList(int userId, String name) {
        int id = 0;
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO lists(name, owner) VALUES(?,?);")) {
            stmt.setString(1, name);
            stmt.setInt(2, userId);
            int rows = stmt.executeUpdate();
            if (rows == 1) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    } else {
                        return Optional.empty();
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in newList!");
        }
        return getList(id);
    }

    public Optional<ItemList> updateList(int listId, String name) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE lists SET name=? WHERE id=?;")) {
            stmt.setString(1, name);
            stmt.setInt(2, listId);
            int rows = stmt.executeUpdate();
            if (rows != 1) {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.err.println("Database error in updateList!");
        }
        return getList(listId);
    }

    public boolean hasList(int userId, int listId) {
        try (PreparedStatement stmt = con.prepareStatement("SELECT id FROM lists WHERE id=? AND owner=?;")) {
            stmt.setInt(1, listId);
            stmt.setInt(2, userId);
            boolean result = stmt.execute();
            if (!result) {
                return false;
            }
            try (ResultSet rs = stmt.getResultSet()) {
                return rs.next();
            }
        } catch (SQLException e) {
            System.err.println("Database error in hasList!");
            return false;
        }
    }

    public Optional<ItemList> getList(int listId) {
        ItemList list;
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name FROM lists WHERE id=?;")) {
            stmt.setInt(1, listId);
            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }

            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }

                int id = rs.getInt("id");
                String name = rs.getString("name");
                list = new ItemList(id, name);
            }
        } catch (SQLException e) {
            System.err.println("Database error in getList!");
            return Optional.empty();
        }
        return Optional.of(list);
    }

    public Optional<Item> newItem(int listId, String name, Frequency frequency) {
        int id = 0;
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO items(name, frequency, parent) VALUES(?,?,?);")) {
            stmt.setString(1, name);
            stmt.setInt(2, frequency.value());
            stmt.setInt(3, listId);
            int rows = stmt.executeUpdate();
            if (rows == 1) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    } else {
                        return Optional.empty();
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in newItem!");
        }
        return getItem(id);
    }

    public Optional<Item> updateItem(int itemId, String name, Frequency frequency) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE items SET name=?, frequency=? WHERE id=?;")) {
            stmt.setString(1, name);
            stmt.setInt(2, frequency.value());
            stmt.setInt(3, itemId);
            int rows = stmt.executeUpdate();
            if (rows != 1) {
                return Optional.empty();
            }
        } catch (SQLException e) {
            System.err.println("Database error in updateItem!");
        }
        return getItem(itemId);
    }

    public Optional<Item> getItem(int itemId) {
        Item item;
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name, completed, frequency, parent FROM items WHERE id=?;")) {
            stmt.setInt(1, itemId);
            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }

            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }

                int id = rs.getInt("id");
                String name = rs.getString("name");
                boolean completed = rs.getBoolean("completed");
                int frequency = rs.getInt("frequency");
                item = new Item(id, name, completed, frequency);
            }
        } catch (SQLException e) {
            System.err.println("Database error in getItem!");
            return Optional.empty();
        }
        return Optional.of(item);
    }

    public Set<Item> getItems(int listId) {
        Set<Item> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name, completed, frequency FROM items WHERE parent=?;")) {
            stmt.setInt(1, listId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String name = rs.getString("name");
                        boolean completed = rs.getBoolean("completed");
                        Frequency frequency = Frequency.of(rs.getInt("frequency"));
                        results.add(new Item(id, name, completed, frequency));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in getItems!");
            return Collections.emptySet();
        }
        return results;
    }

    public boolean completeItem(int itemId, boolean complete) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE items SET completed=? WHERE id=?;")) {
            stmt.setBoolean(1, complete);
            stmt.setInt(2, itemId);
            int rows = stmt.executeUpdate();
            if (rows != 1) {
                return false;
            }
        } catch (SQLException e) {
            System.err.println("Database error in completeItem!");
            return false;
        }
        return true;
    }

    public void archiveLists() {
        try (Statement st = con.createStatement()) {
            st.execute("INSERT INTO history(item_id, date, completed) SELECT id, date, completed FROM items WHERE extract(DAY FROM age(date)) >= 1;\n");
            st.execute("UPDATE items SET completed=false WHERE extract(DAY FROM age(date)) >= 1;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Set<ItemHistory> getHistory(int itemId, int limit) {
        Set<ItemHistory> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT item_id, date, completed FROM history WHERE item_id=? ORDER BY date DESC LIMIT ?;")) {
            stmt.setInt(1, itemId);
            stmt.setInt(2, limit);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        int id = rs.getInt("item_id");
                        String date = rs.getString("date");
                        boolean completed = rs.getBoolean("completed");
                        results.add(new ItemHistory(id, date, completed));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            System.err.println("Database error in getHistory!");
            return Collections.emptySet();
        }
        return results;
    }

    public Set<ItemHistory> getHistory(int itemId) {
        return getHistory(itemId, 50);
    }

    /**
     * Remove everything from the database
     */
    void nuke() {
        try (Statement st = con.createStatement()) {
            st.execute("TRUNCATE history, items, lists, users;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
