package dev.robinsyl.routine.types;

public class Item {
    private int id;
    private String name;
    private boolean completed = false;
    private Frequency frequency = Frequency.DAILY;

    public Item() {
    }

    public Item(int id, String name, boolean completed, Frequency frequency) {
        this.id = id;
        this.name = name;
        this.completed = completed;
        this.frequency = frequency;
    }

    public Item(int id, String name, boolean completed, int frequency) {
        this(id, name, completed, Frequency.of(frequency));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }
}
