package dev.robinsyl.routine.types;

public enum Frequency {
    DAILY(1), MONTHLY(2);

    private final int i;

    Frequency(int i) {
        this.i = i;
    }

    public int value() {
        return i;
    }

    public static Frequency of(int i) {
        switch (i) {
            case 1:
                return DAILY;
            case 2:
                return MONTHLY;
        }
        return DAILY;
    }

    public static Frequency of(String s) {
        switch (s.toLowerCase()) {
            case "daily":
                return DAILY;
            case "monthly":
                return MONTHLY;
        }
        return DAILY;
    }
}
