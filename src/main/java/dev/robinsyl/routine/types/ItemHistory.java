package dev.robinsyl.routine.types;

import java.time.LocalDate;

public class ItemHistory {
    private int itemId;
    private LocalDate date;
    private boolean completed;

    public ItemHistory() {
    }

    public ItemHistory(int itemId, LocalDate date, boolean completed) {
        this.itemId = itemId;
        this.date = date;
        this.completed = completed;
    }

    public ItemHistory(int itemId, String dateString, boolean completed) {
        this(itemId, LocalDate.parse(dateString), completed);
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    private void setDate(String dateString) {
        this.date = LocalDate.parse(dateString);
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
