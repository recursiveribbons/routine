package dev.robinsyl.routine.api;

import dev.robinsyl.routine.db.DatabaseService;
import dev.robinsyl.routine.types.Frequency;
import dev.robinsyl.routine.types.Item;
import dev.robinsyl.routine.types.ItemList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Set;

@Component
@Path("/lists")
@Produces({"application/json"})
@Tag(name = "List")
public class ListEndpoint {

    private final DatabaseService db;

    @Autowired
    public ListEndpoint(DatabaseService db) {
        this.db = db;
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "Retrieve a list")
    public ItemList getList(@PathParam("id") String listId) {
        return db.getList(Integer.valueOf(listId)).orElseThrow(NotFoundException::new);
    }

    @PUT
    @Path("/{id}")
    @Operation(summary = "Update list information")
    public Response updateList(@PathParam("id") String id, @FormParam("name") String name) {
        db.updateList(Integer.valueOf(id), name).orElseThrow(NotFoundException::new);
        return Response.noContent().build();
    }

    @GET
    @Path("/{id}/items")
    @Operation(summary = "Retrieve the items in a list")
    public Set<Item> getItems(@PathParam("id") String listId) {
        return db.getItems(Integer.valueOf(listId));
    }

    @POST
    @Path("/{id}/items")
    @Operation(summary = "Create a new item in a list")
    public Response newItem(@PathParam("id") String listId, @FormParam("name") String name, @FormParam("frequency") String frequency) {
        Item item = db.newItem(Integer.valueOf(listId), name, Frequency.of(frequency)).orElseThrow(NotFoundException::new);
        URI uri = URI.create("/items/" + item.getId());
        return Response.created(uri).build();
    }
}
