package dev.robinsyl.routine.api;

import dev.robinsyl.routine.db.DatabaseService;
import dev.robinsyl.routine.types.ItemList;
import dev.robinsyl.routine.types.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Set;

@Component
@Path("/users")
@Produces({"application/json"})
@Tag(name = "User")
public class UserEndpoint {

    private final DatabaseService db;

    @Autowired
    public UserEndpoint(DatabaseService db) {
        this.db = db;
    }

    @GET
    @Operation(summary = "Retrieves the list of users")
    public Set<User> getUsers() {
        return db.getUsers();
    }

    @POST
    @Operation(summary = "Create a new user")
    public Response newUser(@FormParam("name") String name) {
        User user = db.newUser(name).orElseThrow(NotFoundException::new);
        URI uri = URI.create("/users/" + user.getId());
        return Response.created(uri).build();
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "Get a user by ID")
    public User getUser(@PathParam("id") String userId) {
        return db.getUser(Integer.valueOf(userId)).orElseThrow(NotFoundException::new);
    }

    @PUT
    @Path("/{id}")
    @Operation(summary = "Update user information")
    public Response updateUser(@PathParam("id") String idString, @FormParam("name") String name) {
        int id = Integer.valueOf(idString);
        db.updateUser(id, name).orElseThrow(NotFoundException::new);
        return Response.noContent().build();
    }

    @GET
    @Path("/{id}/lists")
    @Operation(summary = "Get the lists belonging to a user")
    public Set<ItemList> getUserLists(@PathParam("id") String userId) {
        return db.getLists(Integer.valueOf(userId));
    }

    @POST
    @Path("/{id}/lists")
    @Operation(summary = "Create a new list")
    public Response newList(@PathParam("id") String userId, @FormParam("name") String name) {
        ItemList list = db.newList(Integer.valueOf(userId), name).orElseThrow(NotFoundException::new);
        URI uri = URI.create("/lists/" + list.getId());
        return Response.created(uri).build();
    }
}
