package dev.robinsyl.routine.api;

import dev.robinsyl.routine.db.DatabaseService;
import dev.robinsyl.routine.types.Frequency;
import dev.robinsyl.routine.types.Item;
import dev.robinsyl.routine.types.ItemHistory;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Set;

@Component
@Path("/items")
@Produces({"application/json"})
@Tag(name = "Item")
public class ItemEndpoint {

    private final DatabaseService db;

    @Autowired
    public ItemEndpoint(DatabaseService db) {
        this.db = db;
    }

    @GET
    @Path("/{id}")
    @Operation(summary = "Get an item by ID")
    public Item getItem(@PathParam("id") String itemId) {
        return db.getItem(Integer.valueOf(itemId)).orElseThrow(NotFoundException::new);
    }

    @PUT
    @Path("/{id}")
    @Operation(summary = "Edit an item")
    public Response updateItem(@PathParam("id") String itemId, @FormParam("name") String name, @FormParam("frequency") String frequency) {
        db.updateItem(Integer.valueOf(itemId), name, Frequency.of(frequency)).orElseThrow(NotFoundException::new);
        return Response.noContent().build();
    }

    @GET
    @Path("/{id}/history")
    @Operation(summary = "Get the completion history of an item")
    public Set<ItemHistory> getHistory(@PathParam("id") String itemId, @QueryParam("limit") @DefaultValue("50") String limit) {
        return db.getHistory(Integer.valueOf(itemId), Integer.valueOf(limit));
    }

    @POST
    @Path("/{id}/complete")
    @Operation(summary = "Set the completion status of an item")
    public Response completeItem(@PathParam("id") String itemId, @FormParam("complete") @DefaultValue("true") @Parameter(description = "Optional field. Set to 'false' to mark item as incomplete.") String complete) {
        boolean result = db.completeItem(Integer.valueOf(itemId), Boolean.valueOf(complete));
        if (!result) {
            throw new NotFoundException();
        }
        return Response.noContent().build();
    }
}
