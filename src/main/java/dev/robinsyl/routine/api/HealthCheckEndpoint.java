package dev.robinsyl.routine.api;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Component
public class HealthCheckEndpoint {
    @GET
    @Path("/ping")
    public String ping() {
        return "Pong!";
    }
}
