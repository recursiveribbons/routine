package dev.robinsyl.routine.api;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(UserEndpoint.class);
        register(ListEndpoint.class);
        register(ItemEndpoint.class);
        register(HealthCheckEndpoint.class);
    }

}